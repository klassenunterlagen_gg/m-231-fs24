# Leseauftrag - EDÖB - Datenschutz Informationsdossier
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  bis LB1 |
| Ziel | Wichtigste Grundlagen zum Thema Datenschutz lernen |

[Der Eidgenössische Datenschutz- und Öffentlichkeitsbeauftragte (EDÖB), Quelle: Bundeskanzlei](https://www.bk.admin.ch/bk/de/home/bk/organisation-der-bundeskanzlei/eidgenossischer-datenschutz-und-offentlichkeitsbeauftragter-edob.html) stellt Lehrmittel zum Thema Datenschutz zur Verfügung. Die Lehrmittel geben einen umfassenden Einblick in die Bedeutung des Datenschutzes im Umgang mit den neuen Medien. Es hat zum Ziel Lernende für eine verantwortungsvolle Verwendung von Personendaten zum Schutz ihrer Privatsphäre und die Rücksicht auf die Persönlichkeit zu sensibilisieren. 

Zum Einstieg: Schauen und hören Sie sich den Bericht [Überwachungskameras in Mietshäusern nicht in jedem Fall erlaubt](https://www.srf.ch/news/schweiz/schweiz-ueberwachungskameras-in-mietshaeusern-nicht-in-jedem-fall-erlaubt) (18.04.2016, Radio SRF) an.

Das [**Informationsdossier**](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_basismodul.pdf.download.pdf/Lehrmittel%20Basismodul%20DE.pdf) ist zu Beginn dieses IT-Moduls (M231) in drei Abschnitten als Hausaufgabe zu lesen. Der Inhalt des Dossiers ist Teil der Leistungsbeurteilung.

 - **Teil 1**: "Was ist Datenschutz?" <br>(&rArr; bis und mit Seite 9)
 - **Teil 2**: "Moderne Informationstechnologie und ihre Risiken von Internet bis Videotelefonie" <br>(&rArr; bis und mit Seite 23)
 - **Teil 3**: "Moderne Informationstechnologie und ihre Risiken - ab Bilder und Bildrechte" <br>(&rArr; bis und mit Seite 33)

**Tipp:** Sie können für die Leistungsbeurteilung eine Zusammenfassung mitbringen. Fassen Sie das Gelesene laufend zusammen. Das unterstützt das Verständnis und dient automatisch als Prüfungsvorbereitung. 

## 2.1. Unterlagen EDÖB
Alle Unterlagen finden Sie auf der Seite des EDÖB und kann heruntergeladen werden. 

Das Dokument ist im Abschnitt "[Lehrmittel Datenschutz](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_basismodul.pdf.download.pdf/Lehrmittel%20Basismodul%20DE.pdf) (gültig bis 31.08.2023)" zu finden. Es basiert auf dem Datenschutzgesetz, welches bis August 2023 in Kraft war. Ob und wann ein aktualisiertes Lehrmittel vom EDÖB veröffentlicht wird, ist derzeit nicht bekannt.

[Infothek Datenschutz EDÖB](https://www.edoeb.admin.ch/edoeb/de/home/deredoeb/infothek/infothek-ds.html) (&rArr; ganz nach unten scrollen)

### Weiteres

- [Datenschutzgesetz, DSG](https://www.fedlex.admin.ch/eli/fga/2022/1561/de)
- [Datenschutzverordnung, DSV](https://www.fedlex.admin.ch/eli/cc/2022/568/de)

- [Datenschutz Informationsdossier](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_basismodul.pdf.download.pdf/Lehrmittel%20Basismodul%20DE.pdf)
- [Lehrmittel Datenschutz Sek II](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_sek2.pdf.download.pdf/Lehrmittel%20Lektionen%201-7%20Sek%20II%20DE.PDF.pdf)
