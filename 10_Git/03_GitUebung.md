# M231 - Datenschutz und Datensicherheit / Erste Git-Übung

## Einleitung

Sie sollten sich als Informatiker:in dringend mit Git auskennen, denn die meisten Arbeiten in einem Projekt werden in GitHub, GitLab oder BitBucket gespeichert. Als Projekt-Mitarbeiter:in sollten Sie wissen, wie man damit arbeitet. Ausserdem können Sie dieses System auch selber für sich nutzen.

Folgende Schritte und Aufgaben machen Sie bitte und geben am Schluss den Link auf Ihr GitLab-Repository oder GitHub-Repository ab. Im Lernjournal/Dossier zeigen und schreiben Sie Ihr Vorgehen und Ihr Gelerntes auf.


## Vorgehen zu den Installationen

<ol>
  <li>
  Schauen Sie in Ihrem Notebook/PC nach, ob Sie Git schon installiert haben. Das prüfen Sie, wenn Sie die [Windows]-[R] Tastenkombination und schreiben dann dort "**cmd**" hinein und machen [Enter].

![10-01-windows-r.jpg](images/10-01-windows-r.jpg)
![10-01-cmd.jpg](images/10-01-cmd.jpg)

  <br>
  Schreiben Sie auf dem schwarzen Terminal dann **git version** und dann [Enter]

![10-01-git-version.jpg](images/10-01-git-version.jpg) 
 
  <br>
  <br>
  Wenn Git nicht da ist (gibt Fehler), dann installieren Sie es von https://git-scm.com .
Eine gewisse Anleitung finden Sie unter
https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/10_Git 
bzw. unter https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md#eigenen-laptop-f%C3%BCr-die-verwendung-von-git-vorbereiten
<br>
<br>
(aber bitte als Anfänger:in **nicht** Visual Studio Code benutzen, denn das nimmt Ihnen zu viel Arbeit weg und Sie verstehen dann nicht mehr was Sie machen!)

  </li>


  <li>
  Installieren Sie weiter den Editor "**notepad++**" https://notepad-plus-plus.org/downloads  (oder etwas ähnliches)

  </li>


  <li>
  Wenn Sie noch kein Account bei GitLab oder GitHub haben, erstellen Sie online einen neuen Account. Am besten nehmen Sie Ihr richtiger Name dazu. Sie werden diesen Account noch viele Jahre haben.

  </li>


  <li>
  Erstellen Sie im Online-Account ein neues Projekt (oder Repository) mit dem Namen "GitUebung". Machen Sie das Projekt öffentlich und lassen Sie sich auch die Datei README.md mitgenerieren.

![10-04-projekt-anlegen.jpg](images/10-04-projekt-anlegen.jpg)

  </li>


  <li>Kopieren Sie den "Klonen"-Link für HTTPS
  
![10-05-klonen.jpg](images/10-05-klonen.jpg)

  </li>


  <li>
  Gehen Sie nun auf Ihre Hardware (Laptop --> [Windows]-[R] dann "cmd" [Enter] ). Navigieren mit den Befehlen "cd" (für change directory), "mkdir" (make directory) und machen nachher "git clone ......" folgendermassen und sinngemäss:

![10-06-cd-gituebung.jpg](images/10-06-cd-gituebung.jpg)

  </li>


  <li>
  Machen Sie den Laptop mit Ihrem GitLab- (oder GitHub-) Account bekannt.
(Die EMailadresse muss dieselbe sein, wie diejenige, mit welcher Sie den Account eröffnet haben.)

![10-07-global-config.jpg](images/10-07-global-config.jpg)

  </li>


  <li>
  Öffnen Sie Notepad++ und öffnen Sie damit die README.md-Datei vom neu erzeugten Projekt. Löschen Sie im README.md alles ausser die oberste Zeile und beginnen Sie mit den Arbeiten.

![10-08-notepad.jpg](images/10-08-notepad.jpg)

  </li>


  <li>
  Nehmen Sie für die Aufgaben diese Markdown-Referenz(en) zur Hand: 
<br>https://www.markdownguide.org/basic-syntax 
<br>https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

  </li>

</ol>


## Aufgaben

<ul>

  <li>Aufgabe A<br>
Schreiben Sie in README.md auf, was **Git** ist, von wem das erfunden wurde und was man damit machen kann.

  </li>


  <li>Aufgabe B<br>
Schreiben Sie die 5-7 wichtigsten Befehle auf. Machen Sie dazu ein Textbereich, der aussieht wie ein "bash"-Code-Stück (https://itsfoss.com/markdown-code-block).

  </li>


  <li>Aufgabe C<br>
Erstellen Sie eine neue Markdown-Datei (z.B. branches.md) in der Sie die Art beschreiben, was Branches sind und wie man mit Branches umgehen können. Es wird erwartet, dass Sie hier auch ein Bild oder ein Video einbinden. Natürlich müssen Sie ein Link auf diese neue Datei von der README.md aus machen.

  </li>


  <li>Aufgabe D<br>
Erstellen Sie eine weitere neue Markdown-Datei (z.B. links.md) und verlinken Sie diese von der README.md her. In dieser neuen Datei machen Sie eine Links-Sammlung auf Git- und Markdown-Referenzen, vielleicht auch auf Videos, de Sie in Zukunft als Nachschlagewerke benutzen können.

  </li>


  <li>Abschluss-Arbeit<br>
Sichern Sie Ihre Arbeit mittels
 
		git add .
		git commit -m "xxxx"
		git push
	
  </li>
	
  <li>Sie machen ein **Review** bei einem Ihrer Schulkamerad:in und fügen unten in der README.md-Datei **Ihren Namen** und das **Datum** ein. Das kann man auch machen, bevor Ihr:e Schulkamerad:in fertig ist.
<br>(a) machen Sie unten im README.md ein Kapitel "Review" und laden Sieine:e Schulkamerad:in ein, Ihr Projekt zu klonen.
<br>(b) gehen Sie zu einem:r Klassenkamerad:in und machen Sie von dessen "GitUebung"-Projekt ein clone (Achtung! Nicht an der gleichen Stelle wie Ihr Projekt steht, sondt wird vermutlich alles überschrieben!!)

<br><br>

    Sie müssen mind. 1 Review in Ihrem Projekt haben, sonst bekommen Sie einen Abzug.
    (Die Lehrperson schaut bei Ihrer Commit-History nach)

  </li>
	
  <li>Abgabe
<br>
Berechtigen Sie die Lehrperson auf dieses Projekt und geben Sie die Arbeit im TEAMS-Auftrag ab:
<br>- Link auf dieses GitHub- oder GitLab-Projekt (-Repository)
<br>- Link auf Ihr Lernjournal/Dossier -> genau zu diesem Bereich

  </li>

</ul>
